<?php 

class Building {
	// if the access modifier of the property is private, you cannot directly access its value

	// if the access modifier is private the child class won't inherit the properties
	private $name;
	private $floors;
	private $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	// getter function of property name
	public function getName(){
		return $this->name;
	}

	// setter function of property name
	public function setName($name){
		$this->name = $name;
	}

	public function getFloors(){
		return $this->floors;
	}


}

 class Condominium extends Building {

}


$building = new Building('Trial Building', 9, 'Manila City, Manila');

$condominium = new Condominium('Trial Condominium', 100, 'Quezon City, Philippines');

class Drink {
	protected $name;

	public function __construct($name){
		$this->name = $name;
	}

	// getter for the name property
	public function getName(){
		return $this->name;
	}

	// setter for the name property
	public function setName($name){
		$this->name = $name;
	}
}

$milk = new Drink('Alaska');

class Coffee extends Drink {

}

$kopiko = new Coffee('Kopiko');

 ?>