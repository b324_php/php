<?php 
	$tasks = ['get git', 'Bake HTML', 'Eat CSS', 'PHP'];

	// $_GET['index'] - it will get the index data from the GET request.
	// isset($_GET['index']) - will whther the $_GET['index'] variable is existing or not
	if(isset($_GET['index'])){
		// we define indexGet variable and set its value to $_GET['index'] variable
		$indexGet = $_GET['index'];
		echo "The retrieved tasks from GET is $tasks[$indexGet]";
	}

	if(isset($_POST['index'])){
		$indexPost = $_POST['index'];
		echo "The retrieved tas from the POST is $tasks[$indexPost].";
	}
 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S05: Client-Server Communication - Discussion 1</title>
</head>
<body>

	<h1>GET Method:</h1>
	<form method="GET">

        <select name="index" required>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>

        <button type="submit">GET</button>

    </form>
    <!-- <p><?php echo $_GET['index']; ?></p> -->
    <!-- <p><?php var_dump(isset($_GET['index'])) ?></p> -->

    <h1>POST Method:</h1>
    <form method="POST">

        <select name="index" required>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>

        <button type="submit">POST</button>

    </form>

</body>
</html>