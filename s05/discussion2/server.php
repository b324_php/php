<?php 
session_start();

class TaskList {
	public function addTask($description){
		$newTask = (object)[
			'description' => $description,
			'isFinished' => false
		];

		//validation whether the token session is existing or not
		if($_SESSION['tasks'] === null){
			$_SESSION['tasks'] = array();
		}

		array_push($_SESSION['tasks'], $newTask);

	}

	public function update($id, $description, $isFinished){
		$_SESSION['tasks'][$id]->description = $description;
		$_SESSION['tasks'][$id]->isFinished = $isFinished;
	}
}

$tasklist = new TaskList();

if($_POST['action'] === 'add'){
	$tasklist->addTask($_POST['description']);
}else if($_POST['action'] === 'update'){
	$tasklist->update($_POST['id'], $_POST['description'], $_POST['isFinished']);
}else if($_POST['action'] === 'delete'){
	$tasklist->delete($_POST['id'], $_POST['description'], $_POST['isFinished']);

public function delete($id){
	array_splice($_SESSION['tasks'], $id)
}



header('Location: ./index.php');
 ?>
