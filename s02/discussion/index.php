<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02: Selection Control Structures and Array Manipulation</title>
</head>
<body>

	<h1>Repetition Control Structures</h1>

	<h2>While Loop</h2>
	<?php echo whileLoop(); ?>

	<h2>Do-While Loop</h2>
	<?php doWhileLoop(); ?>

	<h2>For Loop</h2>
	<?php forLoop(); ?>

	<h2>Continue and Break</h2>
	<?php modifiedForLoop(); ?>

	<h1>Array Manipulation</h1>

	<h2>Types of Array</h2>

	<h3>Simple Array</h3>

	<ul>
		<!-- php codes/statements can be broken down using the php tags -->
		<?php foreach($computerBrands as $brand){?>
			<!-- PHP inclues a short hand for "php echo tag" -->
			<li><?= $brand; ?></li>

		<?php } ?>
	</ul>

	<h3>Associative Array</h3>

	<ul>
		<?php foreach($gradePeriods as $period => $grade){ ?>
			<li>
				Grade in <?= $period ?> is <?= $grade ?>
			</li>
		<?php } ?>
	</ul>

	<h3>Multidimensional Array</h3>

	<ul>
		<?php 
			foreach($heroes as $team){
				foreach($team as $member){
					?>
						<li><?php echo $member ?></li>
					<?php
				}
			}
		 ?>
	</ul>

	<h3>Multi-Dimentional Associative Array</h3>

	<ul>
		<?php 
			foreach($ironManPowers as $label => $powerGroup){
				foreach($powerGroup as $power){
					?>
						<li><?= "$label: $power" ?></li>
					<?php
				}
			}
		 ?>
	</ul>
</body>
</html>