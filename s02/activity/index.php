<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>A02</title>
</head>
<body>
	
	
	<h3>Divisible By Five</h3>
	<p><?php divisibleByFive(); ?></p>
	
	
	<h3>Array manipulation</h3>
	<?php array_push($students, 'John Smith'); ?>
	<p><?php print_r($students); ?></p>
	<p><?php echo count($students); ?></p>

	<?php array_push($students, 'Jane Smith'); ?>
	<p><?php print_r($students); ?></p>
	<p><?php echo count($students); ?></p>

	<?php array_shift($students); ?>
	<p><?php print_r($students); ?></p>
	<p><?php echo count($students); ?></p>
	

	
</body>
</html>
